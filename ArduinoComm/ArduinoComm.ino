
//TODO: Add accelerometer code

const int xInput = A0;
const int yInput = A1;
const int zInput = A2;

int xRawMin = 512;
int xRawMax = 512;

int yRawMin = 512;
int yRawMax = 512;

int zRawMin = 512;
int zRawMax = 512;

const int buttonPin1a = 2;
const int buttonPin1b = 3;

const int buttonPin2a = 10;
const int buttonPin2b = 11;

const int redLightPin = 12;
//const int greenLightPin = 7;

int status1 = 0;
int buttonState1on = 0;
int buttonState1off = 0;

int status2 = 0;
int buttonState2on = 0;
int buttonState2off = 0;

long threshold = 1000;

const int sampleSize = 10;

void setup() 
{
  analogReference(EXTERNAL);
  Serial.begin(9600);

  pinMode(buttonPin1a, INPUT);
  pinMode(buttonPin1b, INPUT);
  pinMode(buttonPin2a, INPUT);
  pinMode(buttonPin2b, INPUT);
  pinMode(redLightPin, OUTPUT);

  int xRaw = ReadAxis(xInput);
  int yRaw = ReadAxis(yInput);
  int zRaw = ReadAxis(zInput);


  AutoCalibrate(xRaw, yRaw, zRaw);
}
int ReadAxis(int axisPin)
{
  long reading = 0;
  analogRead(axisPin);
  delay(1);
  for (int i = 0; i < sampleSize; i++)
  {
    reading += analogRead(axisPin);
  }
  return (int) reading/sampleSize;
}

void loop() 
{
  buttonState1on = digitalRead(buttonPin1a);
  buttonState1off = digitalRead(buttonPin1b);

  buttonState2on = digitalRead(buttonPin2a);
  buttonState2off = digitalRead(buttonPin2b);


  int xRaw = ReadAxis(xInput);
  int yRaw = ReadAxis(yInput);
  int zRaw = ReadAxis(zInput);


  //delay(1000);
  long xScaled = map(xRaw, xRawMin, xRawMax, -1000, 1000);
  long yScaled = map(yRaw, yRawMin, yRawMax, -1000, 1000);
  long zScaled = map(zRaw, zRawMin, zRawMax, -1000, 1000);
  
  // re-scale to fractional Gs
  int xAccel = (int) xScaled / 1000.0;
  int yAccel = (int) yScaled / 1000.0;
  int zAccel = (int) zScaled / 1000.0;
  
  Serial.println((String) xAccel + "," + (String) yAccel); // + "," + (String)zAccel);
  delay(20);
  //long sensorValue = capSensor.capacitiveSensor(30);

/*
  //==============================================
  //Status 1
  //==============================================
  if (buttonState1on == LOW && status1 == LOW)
  {
    status1 = LOW;
  }
  if (buttonState1on == HIGH && status1 == LOW)
  {
    status1 = HIGH;
  }
  if (buttonState1off == LOW && status1 == HIGH)
  {
    status1 = HIGH;
  }
  if (buttonState1off == HIGH && status1 == HIGH)
  {
    status1 = LOW;
  }  
  //==============================================  
  //Status 2
  //==============================================
  if (buttonState2on == LOW && status2 == LOW)
  {
    status2 = LOW;
  }
  if (buttonState2on == HIGH && status2 == LOW)
  {
    status2 = HIGH;
  }
  if (buttonState2off == LOW && status2 == HIGH)
  {
    status2 = HIGH;
  }
  if (buttonState2off == HIGH && status2 == HIGH)
  {
    status2 = LOW;
  }  
  //==============================================
  //Status 3
  //==============================================  
  if (xAccel > 500)
  {
    digitalWrite(redLightPin, HIGH);
  }
  if (xAccel > 100)
  {
    digitalWrite(redLightPin, LOW);
  }
    //status1 = LOW;
    //status2 = LOW;
    //Serial.println("Abort");

  //Serial.println(sensorValue);
*/
  //Control Panel Code
  //Serial.println((String) status1 + "," + (String) status2 + "," + "0" + "," + "0" + "," + (int) xScaled + "," + (int) yScaled); 

  //Control Ship Code


  int x;
  int y;
  int z;
    
  if (xScaled < -1000)
  {
    x = 1;
  }
  else
  {
    x = 0;
  }
  if (yScaled < -1000)
  {
    y = 1;
  }
  else if (yScaled > -1000)
  {
    y = 0;
  }
  if (zScaled < -1000)
  {
    z = 1;
  }
  else if (zScaled > -1000)
  {
    z = 0;
  }
  
  //Serial.println((String)xScaled + "," + (String)yScaled + "," + (String)zScaled);
  //Serial.println((String)x + "," + (String)y + "," + (String)z);
  //delay(10);
}


//
// Read "sampleSize" samples and report the average
//

//
// Find the extreme raw readings from each axis
//
void AutoCalibrate(int xRaw, int yRaw, int zRaw)
{
  //Serial.println("Calibrate");
  if (xRaw < xRawMin)
  {
    xRawMin = xRaw;
  }
  if (xRaw > xRawMax)
  {
    xRawMax = xRaw;
  }
  
  if (yRaw < yRawMin)
  {
    yRawMin = yRaw;
  }
  if (yRaw > yRawMax)
  {
    yRawMax = yRaw;
  }

  if (zRaw < zRawMin)
  {
    zRawMin = zRaw;
  }
  if (zRaw > zRawMax)
  {
    zRawMax = zRaw;
  }
}
