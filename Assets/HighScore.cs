﻿using UnityEngine;
using System.Collections;

public class HighScore : MonoBehaviour {
	// Use this for initialization
	UnityEngine.UI.Text uiText;
	void Start () 
	{
		uiText = GetComponent<UnityEngine.UI.Text> ();
	}

	// Update is called once per frame
	void Update () 
	{
		uiText.text = "High Score:" + (PlayerPrefs.GetInt("HighScore")).ToString();
	}
}
