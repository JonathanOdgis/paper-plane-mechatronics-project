﻿using UnityEngine;
using System.Collections;

public class Narrator : MonoBehaviour {

	// Use this for initialization
	public static Narrator Instance;
	public AudioSource audioSrc;
	public AudioClip[] gameOverAudio;
	public AudioClip[] gameOverHighScoreAudio;
	public bool hasPlayed;
	void Start () 
	{
		Instance = this;
		audioSrc = GetComponent<AudioSource> ();
		//audioSrc.clip = gameOverAudio[Random.Range(0, gameOverAudio.Length)];
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (PlayerAttributes.Instance.health <= 0) 
		{
			//TODO: IF HIGH SCORE ISNT BEATEN PLAY RANDOM GAME OVER AUDIO
			//audioSrc.clip = gameOverAudio;
			if (!audioSrc.isPlaying && !hasPlayed) 
			{
				audioSrc.PlayOneShot (audioSrc.clip);
				hasPlayed = true;
			}
			//TODO: ELSE IF HIGH SCORE IS BEATEN PLAY RANDOM HIGH SCORE AUDIO
		}	
	}

	public void SetAudioClip()
	{
		if (PlayerAttributes.Instance.isHighScore)
			audioSrc.clip = gameOverHighScoreAudio[Random.Range(0, gameOverHighScoreAudio.Length)];
		else
			audioSrc.clip = gameOverAudio[Random.Range(0, gameOverAudio.Length)];			
	}
		
}
