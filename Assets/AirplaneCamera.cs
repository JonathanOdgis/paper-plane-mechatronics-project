﻿using UnityEngine;
using System.Collections;

public class AirplaneCamera : MonoBehaviour {

	// Use this for initialization
	public static AirplaneCamera Instance;
	public Transform target;
	private bool isRecalibrating;
	void Start () 
	{
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (target.transform.eulerAngles.y <= 90 && target.transform.eulerAngles.y > 0) 
		{
			this.transform.position = Vector3.MoveTowards (this.transform.position, new Vector3(target.transform.position.x - 10, target.transform.position.y, target.transform.position.z - 10), 1000 * Time.deltaTime);
		} 
		if (target.transform.eulerAngles.y < 180 && target.transform.eulerAngles.y > 90) 
		{
			this.transform.position = Vector3.MoveTowards (this.transform.position, new Vector3(target.transform.position.x - 10, target.transform.position.y, target.transform.position.z - 10), 1000 * Time.deltaTime);		
		}
		else 
		{
			this.transform.position = Vector3.MoveTowards (this.transform.position, new Vector3(this.transform.position.x, target.transform.position.y, target.transform.position.z - 10), 1000 * Time.deltaTime);
		}
		this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, target.transform.eulerAngles.y, target.transform.eulerAngles.z);  

	}


}
