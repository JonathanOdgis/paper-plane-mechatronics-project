﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Health : MonoBehaviour 
{
	public GameObject healthUnitIcon;
	public GameObject[] healthIcons;
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*
		Debug.Log (ClosestArrayIndex());
		if (PlayerAttributes.Instance.health < ClosestArrayIndex()) 
		{
			Destroy (healthIcons [ClosestArrayIndex()].gameObject);
		}
		*/
	
		if (PlayerAttributes.Instance.health < 3) 
		{
			Destroy (healthIcons [2].gameObject);
		}
		if (PlayerAttributes.Instance.health < 2) 
		{
			Destroy (healthIcons [1].gameObject);
		}
		if (PlayerAttributes.Instance.health < 1) 
		{
			Destroy (healthIcons [0].gameObject);
		}
	}

	int ClosestArrayIndex()
	{
		int count = healthIcons.Length-1;
		bool foundElement = false;
		while (!foundElement) 
		{
			if (healthIcons [count].gameObject == null) 
			{
				Debug.Log (healthIcons [count].gameObject.name + " is null");
				count--;
			}
			if (healthIcons [count].gameObject != null) 
			{
				foundElement = true;
			} 
			if (count < 0) 
			{
				return 0;
			}
		}
		return count;
	}
}
