﻿using UnityEngine;
using System.Collections;

public class timer : MonoBehaviour {

	// Use this for initialization
	UnityEngine.UI.Text uiText;
	void Start () 
	{
		uiText = GetComponent<UnityEngine.UI.Text> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		uiText.text = ((int)PlayerAttributes.Instance.time).ToString();
	}
}
