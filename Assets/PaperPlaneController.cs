﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using System.IO.Ports;
using UnityEngine.SceneManagement;

public class PaperPlaneController : MonoBehaviour 
{
	public static PaperPlaneController Instance;
	public SerialPort serialPort = new SerialPort ("COM3", 9600);  //COM4 on desktop
	public bool useArduino;	
	public int xInput = 0;
	public int yInput = 0;

	public GameObject model;
	public Rigidbody rgb;
	private AudioSource audioSrc;
	bool hasPlayedLeft;
	bool hasPlayedRight;
	public AudioClip turnSound;
	public AudioClip defaultSound;
	public AudioClip gameOverSound;
	private ConstantForce constFrc;
	public float speed;
	public float facingForwardX;
	public float facingForwardY;
	public float facingForwardZ;
	public float xOffset;
	public float zOffset;
	public bool autoZMode;
	public bool autoXMode;
	public bool isGameMode;
	// Use this for initialization
	void Awake() 
	{
		Instance = this;
		if (useArduino) {
			try {
				Debug.Log ("Port Open");
				serialPort.Open ();
				serialPort.DataReceived += DataReceivedHandler;
			} catch (UnityException e) {
				Debug.Log ("Could not find the serial port" + e.Message);
			}
		}
		rgb = GetComponent<Rigidbody> ();
		audioSrc = GetComponent<AudioSource> ();
		constFrc = GetComponent<ConstantForce> ();
		facingForwardX = this.transform.eulerAngles.x;
		facingForwardY = this.transform.eulerAngles.y;
		facingForwardZ = this.transform.eulerAngles.z;
	}


	private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
	{
		Debug.Log ("Data Received");
		SerialPort sp = (SerialPort)sender;
		string distance = sp.ReadLine();
		Debug.Log(distance);
	}

	void CheckFornewArduinoInput()
	{
		string data = serialPort.ReadLine ();
		if (useArduino) 
		{
			if (data.Substring (0, 1).Contains ("-"))
				xInput = -int.Parse (data.Substring (1, 1));
			else
				xInput = int.Parse (data.Substring (0, 1));
			
			if (data.Substring (3, 1).Contains ("-"))
				yInput = -int.Parse (data.Substring (4, 1));
			else
				yInput = int.Parse (data.Substring (3, 1));
		}
		//Debug.Log (data);
	

	}

	void FixedUpdate()
	{
		if (this.transform.eulerAngles.y > 60) {
			autoZMode = false;
			autoXMode = true;
		} 
		else 
		{
			autoZMode = true;
			autoXMode = false;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (useArduino) {
			CheckFornewArduinoInput ();
			serialPort.BaseStream.ReadTimeout = 15;
		}
			//auto move towards X axis


		//auto move towards Z axis
		this.transform.eulerAngles = Vector3.MoveTowards(this.transform.eulerAngles, new Vector3(facingForwardX, facingForwardY, facingForwardZ), 100 * Time.deltaTime);

		if (autoZMode)
			rgb.velocity = new Vector3 (rgb.velocity.x, rgb.velocity.y, speed);
		if (autoXMode)
			rgb.velocity = new Vector3 (speed, rgb.velocity.y, rgb.velocity.z);
		//rgb.velocity.Set (rgb.velocity.x, 0, rgb.velocity.z);


		if (CrossPlatformInputManager.GetAxis ("Horizontal") < 0 || (useArduino && xInput < 0)) 
		{
			if (!useArduino)
				rgb.AddForce(-transform.right * 100);
			else
				rgb.AddForce(-transform.right * 100);  //30 original
			model.transform.eulerAngles = new Vector3(model.transform.eulerAngles.x, model.transform.eulerAngles.y, 30); //Vector3.MoveTowards(model.transform.eulerAngles, new Vector3(model.transform.eulerAngles.x, model.transform.eulerAngles.y, 10), 50 * Time.deltaTime); 

		}
		if (CrossPlatformInputManager.GetAxis("Horizontal") > 0 || (useArduino && xInput > 0))
		{
			if (!useArduino)
				rgb.AddForce(transform.right * 100 );
			else
			{
				rgb.AddForce(transform.right * 100);
			}
			Debug.Log("Make the right animation happen");
			model.transform.eulerAngles =  new Vector3(model.transform.eulerAngles.x, model.transform.eulerAngles.y, -30); //Vector3.MoveTowards(model.transform.eulerAngles, new Vector3(model.transform.eulerAngles.x, model.transform.eulerAngles.y, -10), 50 * Time.deltaTime); 
		}

		if (CrossPlatformInputManager.GetAxis ("Vertical") > 0 || (useArduino && yInput < 0)) 
		{
			//rgb.AddForce(Vector3.down * 50);
			if (!useArduino)
				constFrc.force = Vector3.down * 100;
			else
				rgb.AddForce(Vector3.down * 100); //constFrc.force = Vector3.down * 1000;
			model.transform.eulerAngles = Vector3.MoveTowards(model.transform.eulerAngles, new Vector3(20+xOffset, model.transform.eulerAngles.y, model.transform.eulerAngles.z), 50 * Time.deltaTime); 
		}
		if (CrossPlatformInputManager.GetAxis ("Vertical") < 0 || (useArduino && yInput > 0)) 
		{
			if (!useArduino)
				constFrc.force = Vector3.up * 100;
			else
				rgb.AddForce(Vector3.up * 100); 
			//model.transform.eulerAngles = Vector3.MoveTowards(model.transform.eulerAngles, new Vector3(-20+xOffset, model.transform.eulerAngles.y, model.transform.eulerAngles.z), 50 * Time.deltaTime); 
		} 
		if (CrossPlatformInputManager.GetAxis("Vertical") == 0 && !useArduino) //|| (useArduino && yInput == 0))
		{
			constFrc.force = Vector3.zero;
			model.transform.eulerAngles = new Vector3(0+xOffset, model.transform.eulerAngles.y, model.transform.eulerAngles.z);
		}
		if (CrossPlatformInputManager.GetAxis ("Horizontal") == 0 && !useArduino) // useArduino) //|| (useArduino && xInput == 0))
		{
			model.transform.eulerAngles = new Vector3(model.transform.eulerAngles.x, model.transform.eulerAngles.y, 0);
		}
		//Debug.Log (rgb.velocity);
		if (rgb.velocity.x > 0 && rgb.velocity.x < 15 && !hasPlayedLeft) 
		{
			if(!audioSrc.isPlaying )
			{
				audioSrc.PlayOneShot(turnSound);
				hasPlayedLeft = true;
				hasPlayedRight = false;
			}
		}
		if (rgb.velocity.x < 0 && rgb.velocity.x > -15 && !hasPlayedRight) {
			if (!audioSrc.isPlaying) 
			{
				audioSrc.PlayOneShot (turnSound);
				hasPlayedRight = true;
				hasPlayedLeft = false;
			}
		}
			
		//Debug.Log (data);
		//Debug.Log (xInput + "," + yInput);
	}

	IEnumerator GameOverState()
	{
		if (PlayerPrefs.GetInt ("HighScore") < PlayerAttributes.Instance.time) 
		{
			PlayerAttributes.Instance.isHighScore = true;
			PlayerPrefs.SetInt ("HighScore", (int) PlayerAttributes.Instance.time);
		}

		Narrator.Instance.SetAudioClip ();
		Destroy (model);
		rgb.velocity = new Vector3 (0, 0, 0);		
		yield return new WaitForSeconds (.5f);
		serialPort.Close ();
		yield return new WaitForSeconds (.2f);
		yield return new WaitForSeconds (Narrator.Instance.audioSrc.clip.length);
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("AngleTrigger"))
        {
			if (other.GetComponent<ObjectRotateTrigger> ().desiredYDirection <= 90 && other.GetComponent<ObjectRotateTrigger> ().desiredYDirection > 0) {
				xOffset = other.GetComponent<ObjectRotateTrigger> ().desiredXDirection;
				zOffset = other.GetComponent<ObjectRotateTrigger> ().desiredZDirection;

			}

			facingForwardX = other.GetComponent<ObjectRotateTrigger>().desiredXDirection;//transform.eulerAngles.x;
			facingForwardY = other.GetComponent<ObjectRotateTrigger>().desiredYDirection;//transform.eulerAngles.y;
			facingForwardZ = other.GetComponent<ObjectRotateTrigger>().desiredZDirection;//transform.eulerAngles.z;
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag ==  "Obstacle")   //Health is less than or equal to 0
		{
			PlayerAttributes.Instance.health--;
			if (PlayerAttributes.Instance.health <= 0) 
			{
				StartCoroutine (GameOverState ());
			} 
			else 
			{
				//Play crumpling up animation
			}
			other.gameObject.GetComponentInChildren<Collider> ().enabled = false;
		}
	}
}
