﻿using UnityEngine;
using System.Collections;

public class PlayerAttributes : MonoBehaviour {

	// Use this for initialization
	public static PlayerAttributes Instance;
	public float time;
	public float timeItTookToStart;
	public float health = 3;
	public bool started;
	public bool isHighScore;
	void Start () 
	{
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!started)
		{
			if (PaperPlaneController.Instance.rgb.velocity.z < 100) 
			{
				timeItTookToStart = Time.timeSinceLevelLoad;
				if (timeItTookToStart > 10) 
				{
					//Debug.Log ("You filthy cheater. You think I didn't consider that you'd exploit the game this way?");
				}
			} 
			else
			{
				timeItTookToStart = -timeItTookToStart;
				started = true;
			}
		}
		if (health > 0)
		{
			time = Time.timeSinceLevelLoad + timeItTookToStart;// - TimeItTookToStart;
		}
	}
}
