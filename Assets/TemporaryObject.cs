﻿using UnityEngine;
using System.Collections;

public class TemporaryObject : MonoBehaviour 
{

	public float manualDelay;
	public new ParticleSystem particleSystem;
	private AudioSource audioSrc;
	public AudioClip audioClip;
	public float maxDistance = 20;
	// Use this for initialization

	void Start () 
	{

	}

	void Awake()
	{
		//Debug.Log ("Awake");
		audioSrc = GetComponent<AudioSource> ();
		PlaySound ();
		StartCoroutine(DestroyEffect());

	}

		// Update is called once per frame

	void PlaySound()
	{
		if (audioSrc != null)
		{
			if (!audioSrc.isPlaying && Vector2.Distance(PlayerAttributes.Instance.gameObject.transform.position, this.transform.position) < maxDistance)
				audioSrc.PlayOneShot(audioClip);
			else
				audioSrc.Stop ();	
		}
	}

	IEnumerator DestroyEffect()
	{
		//Debug.Log ("Destroy Effect");
		if (particleSystem != null && manualDelay == 0) 
			yield return new WaitForSeconds (particleSystem.duration + 1);
		else if (particleSystem != null && manualDelay > 0) {
			yield return new WaitForSeconds (manualDelay);
		} 
		else {
			yield return new WaitForSeconds (manualDelay);
		}
		Destroy (this.gameObject);

	}
}
