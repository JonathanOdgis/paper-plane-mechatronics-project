﻿using UnityEngine;
using System.Collections;

public class GameOverText : MonoBehaviour 
{
	UnityEngine.UI.Text uiText;

	void Start()
	{
		uiText = GetComponent<UnityEngine.UI.Text> ();
		uiText.enabled = false;
	}

	void Update()
	{
		if (PlayerAttributes.Instance.health <= 0) 
		{
			uiText.enabled = true;
		}
	}
}
