﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	public GameObject[] Obstacles;
	public GameObject Terrain;
	public GameObject[] Spawners;
	public bool isSpawning = true;
	// Use this for initialization
	void Start () 
	{
		StartCoroutine (SpawnObject ());
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate (transform.forward * 150 * Time.deltaTime);
	}

	IEnumerator SpawnObject()
	{
		while (isSpawning) 
		{
			GameObject spawner = Spawners [Random.Range (0, Spawners.Length - 1)];
			GameObject clone = Instantiate (Obstacles [Random.Range (0, Obstacles.Length - 1)].gameObject, spawner.transform.position, spawner.transform.rotation) as GameObject; 
			yield return new WaitForSeconds (Random.Range (1, 5));
		}
	}
}
