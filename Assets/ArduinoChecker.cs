﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using UnityEngine.SceneManagement;

public class ArduinoChecker : MonoBehaviour 
{
	public string levelToLoad;
	void Awake() 
	{
		PaperPlaneController.Instance.speed = 0;
	}

	// Update is called once per frame
	void Update () 
	{				
		if ((PaperPlaneController.Instance.xInput >= 5) || (PaperPlaneController.Instance.yInput >= 5)) 
		{
			StartCoroutine(ChangeSceneState ());
		}	
	}

	IEnumerator ChangeSceneState()
	{
		//TODO Make Text Dissappear
		PaperPlaneController.Instance.speed = 20;
		yield return new WaitForSeconds (3f);
		PaperPlaneController.Instance.serialPort.Close ();
		SceneManager.LoadScene (levelToLoad);
	}
}

